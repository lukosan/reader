
<h1>The Sapir-Whorf Hypothesis</h1> 
<p>

<h2>Daniel Chandler</h2>
<P>

<p>

Within linguistic theory, two extreme positions concerning the relationship 
between language and thought are commonly referred to as 'mould theories’ and 
'cloak theories'. <i>Mould theories</i> represent language as 'a mould in terms of which 
thought categories are cast' (Bruner et al. 1956, p. 11). <i>Cloak theories</I> represent 
the view that 'language is a cloak conforming to the customary categories of 
thought of its speakers' (ibid.). The doctrine that language is the 'dress of 
thought' was fundamental in Neo-Classical literary theory (Abrams 1953, p. 
290), but was rejected by the Romantics (ibid.; Stone 1967, Ch. 5). There is also 
a related view (held by behaviourists, for instance) that language and thought are 
<i>identical</i>. According to this stance thinking is entirely linguistic: there is no 
'non-verbal thought', no 'translation' at all from thought to language. In this sense, 
thought is seen as completely determined by language.<p>
	The Sapir-Whorf theory, named after the American linguists Edward Sapir 
and Benjamin Lee Whorf, is a <i>mould</i> theory of language. Writing in 1929, Sapir 
argued in a classic passage that:<p>
<ul>

Human beings do not live in the objective world alone, nor alone in the world 
of social activity as ordinarily understood, but are very much at the mercy of 
the particular language which has become the medium of expression for their 
society. It is quite an illusion to imagine that one adjusts to reality 
essentially without the use of language and that language is merely an 
incidental means of solving specific problems of communication or 
reflection. The fact of the matter is that the 'real world' is to a large extent 
unconsciously built upon the language habits of the group. No two 
languages are ever sufficiently similar to be considered as representing the 
same social reality. The worlds in which different societies live are distinct 
worlds, not merely the same world with different labels attached... We see 
and hear and otherwise experience very largely as we do because the 
language habits of our community predispose certain choices of 
interpretation. (Sapir 1958 [1929], p. 69)<p>
</ul>

This position was extended in the 1930s by his student Whorf, who, in another 
widely cited passage, declared that:<p>
<ul>

We dissect nature along lines laid down by our native languages. The 
categories and types that we isolate from the world of phenomena we do not 
find there because they stare every observer in the face; on the contrary, the 
world is presented in a kaleidoscopic flux of impressions which has to be 
organized by our minds - and this means largely by the linguistic systems in 
our minds. We cut nature up, organize it into concepts, and ascribe 
significances as we do, largely because we are parties to an agreement to 
organize it in this way - an agreement that holds throughout our speech 
community and is codified in the patterns of our language. The agreement is, 
of course, an implicit and unstated one, <i>but its terms are absolutely 
obligatory</i>; we cannot talk at all except by subscribing to the organization 
and classification of data which the agreement decrees. (Whorf 1940, pp. 
213-14; his emphasis)<p>
</ul>

I will not attempt to untangle the details of the personal standpoints of Sapir and 
Whorf on the degree of determinism which they felt was involved, although I 
think that the above extracts give a fair idea of what these were. I should note that 
Whorf distanced himself from the behaviourist stance that thinking is entirely 
linguistic (Whorf 1956, p. 66). In its most extreme version 'the Sapir-Whorf 
hypothesis' can be described as consisting of two associated principles. According 
to the first, <i>linguistic determinism</i>, our thinking is determined by language. 
According to the second, <i>linguistic relativity</i>, people who speak different 
languages perceive and think about the world quite differently.<p>
	On this basis, the Whorfian perspective is that translation between one 
language and another is at the very least, problematic, and sometimes impossible. 
Some commentators also apply this to the 'translation' of unverbalized thought 
into language. Others suggest that even within a single language <i>any</i> 
reformulation of words has implications for meaning, however subtle. George 
Steiner (1975) has argued that <i>any</i> act of human communication can be seen as 
involving a kind of translation, so the potential scope of Whorfianism is very 
broad indeed. Indeed, seeing reading as a kind of translation is a useful reminder 
of the reductionism of representing textual reformulation simply as a determinate 
'change of meaning', since meaning does not reside <i>in</i> the text, but is generated 
by <I>interpretation</i>. According to the Whorfian stance, 'content' is bound up with 
linguistic 'form', and the use of the medium contributes to shaping the meaning. 
In common usage, we often talk of different verbal formulations 'meaning the 
same thing'. But for those of a Whorfian persuasion, such as the literary theorist 
Stanley Fish, 'it is impossible to mean the same thing in two (or more) different 
ways' (Fish 1980, p. 32). Re<i>form</i>ulating something trans<i>forms</i> the ways in which 
meanings may be made with it, and in this sense, form and content are 
inseparable. From this stance words are not merely the 'dress' of thought.<p>
	The importance of what is 'lost in translation' varies, of course. The issue is 
usually considered most important in literary writing. It is illuminating to note 
how one poet felt about the translation of his poems from the original Spanish into 
other European languages (Whorf himself did not in fact regard European 
languages as significantly different from each other). Pablo Neruda noted that the 
best translations of his own poems were Italian (because of its similarities to 
Spanish), but that English and French 'do not correspond to Spanish - neither in 
vocalization, or in the placement, or the colour, or the weight of words.' He 
continued: 'It is not a question of interpretative equivalence: no, the sense can be 
right, but this correctness of translation, of meaning, can be the destruction of a 
poem. In many of the translations into French - I don't say in all of them - my 
poetry escapes, nothing remains; one cannot protest because it says the same 
thing that one has written. But it is obvious that if I had been a French poet, I 
would not have said what I did in that poem, because the value of the words is so 
different. I would have written something else' (Plimpton 1981, p. 63). With more 
'pragmatic' or less 'expressive' writing, meanings are typically regarded as less 
dependent on the particular form of words used. In most pragmatic contexts, 
paraphrases or translations tend to be treated as less fundamentally problematic. 
However, even in such contexts, particular words or phrases which have an 
important function in the original language may be acknowledged to present 
special problems in translation. Even outside the humanities, academic texts 
concerned with the social sciences are a case in point.<p>
	The Whorfian perspective is in strong contrast to the extreme <i>universalism</i> 
of those who adopt the <i>cloak</i> theory. The Neo-Classical idea of language as 
simply the dress of thought is based on the assumption that the same thought can 
be expressed in a variety of ways. Universalists argue that we can say whatever 
we want to say in any language, and that whatever we say in one language can 
always be translated into another. This is the basis for the most common 
refutation of Whorfianism. 'The fact is,' insists the philosopher Karl Popper, 'that 
even totally different languages are not untranslatable' (Popper 1970, p. 56). The 
evasive use here of 'not untranslatable' is ironic. Most universalists do 
acknowledge that translation may on occasions involve a certain amount of 
circumlocution.<p>
	Individuals who regard writing as fundamental to their sense of personal and 
professional identity may experience their written style as inseparable from this 
identity, and insofar as writers are 'attached to their words', they may favour a 
Whorfian perspective. And it would be hardly surprising if individual stances 
towards Whorfianism were not influenced by allegiances to Romanticism or 
Classicism, or towards either the arts or the sciences. As I have pointed out, in the 
context of the written word, the 'untranslatability' claim is generally regarded as 
strongest in the arts and weakest in the case of formal scientific papers (although 
rhetorical studies have increasingly blurred any clear distinctions). And within the 
literary domain, 'untranslatability' was favoured by Romantic literary theorists, 
for whom the connotative, emotional or personal meanings of words were crucial 
(see Stone 1967, pp. 126-7, 132, 145).<p>
	Whilst few linguists would accept the Sapir-Whorf hypothesis in its 'strong', 
extreme or deterministic form, many now accept a 'weak', more moderate, or 
limited Whorfianism, namely that the ways in which we see the world may be 
<i>influenced</i> by the kind of language we use. <i>Moderate Whorfianism</i> differs from 
extreme Whorfianism in these ways:<p>

<ul>
<li>the emphasis is on the potential for thinking to be 'influenced' rather than 
unavoidably 'determined' by language;<br>
<li>it is a two-way process, so that 'the kind of language we use' is also 
influenced by 'the way we see the world';<br>
<li>any influence is ascribed not to 'Language' as such or to one language 
compared with another, but to the use <i>within a language</i> of one variety 
rather than another (typically a <i>sociolect</i> - the language used primarily by 
members of a particular social group);<br>
<li>emphasis is given to the social context of language use rather than to purely 
linguistic considerations, such as the social pressure in particular contexts to 
use language in one way rather than another.<p>
</ul>

Of course, some polemicists still favour the notion of language as a <i>strait-jacket</i> 
or <i>prison</i>, but there is a broad academic consensus favouring moderate 
Whorfianism. Any linguistic influence is now generally considered to be related 
not primarily to the formal systemic structures of a language (<i>langue</i> to use de 
Saussure's term) but to cultural conventions and individual styles of use (or 
<i>parole</i>). Meaning does not reside <i>in</i> a text but arises in 
its interpretation, and interpretation is shaped by sociocultural contexts. 
Conventions regarding what are considered appropriate uses of language in 
particular social contexts exist both in 'everyday' uses of language and in 
specialist usage. In academia, there are general conventions as well as particular 
ones in each disciplinary and methodological context. In every subculture, the 
dominant conventions regarding appropriate usage tend to exert a conservative 
influence on the framing of phenomena. From the media theory perspective, the 
<i>sociolects</i> of sub-cultures and the <i>idiolects</i> of individuals represent a subtly 
selective view of the world: tending to <i>support</i> certain kinds of observations and 
interpretations and to <i>restrict</i> others. And this transformative power goes largely 
unnoticed, retreating to transparency.<p>
	Marshall McLuhan argued in books such as <i>The Gutenberg Galaxy</i> (1962) 
and <i>Understanding Media</i> (1964) that the use of new media was the prime cause 
of fundamental changes in society and the human psyche. The technological 
determinism of his stance can be seen as an application of extreme Whorfianism 
to the nature of media in general. Similarly, the extreme universalism of the cloak 
theorists has its media counterpart in the myth of <i>technological neutrality</i> 
(Winner 1977; Bowers 1988). My own approach involves exploring the 
applicability of moderate Whorfianism to the use of media.<p>

<h3>References</h3>
<p>
<ul>
<li>Abrams, M. H. (1953): <i>The Mirror and the Lamp: Romantic Theory and the Critical 
Tradition</I>. Oxford: Oxford University Press<br>
<li>Bowers, C. A. (1988): <i>The Cultural Dimensions of Educational Computing: 
Understanding the Non-Neutrality of Technology</I>. New York: Teachers College Press<br>
<li>Bruner, J. S., J. S. Goodnow & G. A. Austin ([1956] 1962): <i>A Study of Thinking</I>. New 
York: Wiley<br>
<li>Fish, S. (1980): <i>Is There a Text in This Class? The Authority of Interpretative 
Communities</i>. Cambridge, MA: Harvard University Press<br>
<li>McLuhan, M. (1962): <i>The Gutenberg Galaxy: The Making of Typographic Man</i>. 
London: Routledge & Kegan Paul<br>
<li>McLuhan, M. (1964): <i>Understanding Media: The Extensions of Man</i>. New York: 
McGraw-Hill<br>
<li>Plimpton, G. (ed.) (1963-1988): <i>Writers at Work: The 'Paris Review' Interviews</i>, 
Vol. 5, 1981. London: Secker & Warburg/ Harmondsworth: Penguin (pagination differs)<br>
<li>Popper, K. (1970): 'Normal Science and its Dangers'. In I. Lakatos & A. Musgrave (eds.) 
(1970): <i>Criticism and the Growth of Knowledge</I>. 
London: Cambridge University Press<br>
<li>Sapir, E. (1929): 'The Status of Linguistics as a Science'. In E. Sapir (1958): <i>Culture, 
Language and Personality</i> (ed. D. G. Mandelbaum). Berkeley, CA: University of 
California Press<br>
<li>Steiner, G. (1975): <i>After Babel: Aspects of Language and Translation</i>. London: Oxford 
University Press<br>
<li>Stone, P. W. K. (1967): <i>The Art of Poetry 1750-1820: Theories of Poetic Composition 
and Style in the Late Neo-Classic and Early Romantic Periods</i>. London: Routledge & 
Kegan Paul<br>
<li>Whorf, B. L. (1940): 'Science and Linguistics', <i>Technology Review</i> <b>42</b>(6): 229-31, 
247-8. Also in B. L. Whorf (1956): <i>Language, Thought and Reality</i> (ed. J. B. Carroll). Cambridge, 
MA: MIT Press<br>
<li>Winner, L. (1977): <i>Autonomous Technology: Technics-Out-Of-Control as a Theme in 
Political Thought</i>. Cambridge, MA: MIT Press
<p>
</ul>

Daniel Chandler<BR>
UWA 1994<P>

(Adapted from my book, 
<A HREF="http://users.aber.ac.uk/dgc/act.html">
<I>The Act of Writing</i></A>)
<p>
